## git的配置
1. 安装完成git后

2. 输入ssh-keygen -t rsa -b 4096 -C “邮箱名” 然后连按三次enter
![1.png](E:/code/c++/1.png)

3. $ cat ~/.ssh/id_rsa.pub 打开公钥

   ![](E:\code\c++\4.png)

4. 复制出来的公钥到 gitlab github gitee……

   ![](E:\code\c++\3.png)

5. $ ssh -T git@gitee.com 查看是否配置成功

    

6. 出现
    
    This key is not known by any other names
    Are you sure you want to continue connecting (yes/no/[fingerprint])?
    
7. ![2.png](E:\code\c++\2.png)

输入yes 即可

8. 若返回 Hi XXX! You’ve successfully authenticated, but Gitee.com does not provide shell access. 内容，则证明添加成功。

